from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import os, sys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import WebDriverWait
import time
from selenium.webdriver.chrome.options import Options
import urllib
from urllib.request import urlopen
import os.path
import time

def downloadCSV(query, perimeter, description, path, pathToSaveResult):

    options = webdriver.ChromeOptions()
    # windows_useragent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36"
    linux_useragent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36"
    options.add_argument("--disable-blink-features=AutomationControlled")
    options.add_argument("--no-sandbox")
    options.add_argument("user-agent=#{linux_useragent}")
    options.add_argument("--disable-web-security")
    options.add_argument("--disable-xss-auditor")
    
    driver = webdriver.Chrome(options=options, executable_path=path)
    resource_url="https://www.scopus.com/search/form.uri?display=advanced"


    driver.get(resource_url)

    try:
        close_banner=driver.find_element_by_id("_pendo-close-guide_")
        close_banner.click()
    except:
        None

    search_bar = driver.find_element_by_id("searchfield")
    search_bar.send_keys(" ")

    try:
        close_banner=driver.find_element_by_id("_pendo-close-guide_")
        close_banner.click()
    except:
        None

    time.sleep(4.0)

    sys.stdout.write ("\nQuery length: "+str(len(query))+" characters")

    query = str(query)
    query = query.replace("\"","\\\"",10000000000)

    driver.execute_script("var exampleInput = document.getElementById('searchfield'); exampleInput.innerText = \"" + query + "\" ; exampleInput.dispatchEvent(new Event('change'));");

    sys.stdout.write("\nPerforming the query")

    search_bar.send_keys(Keys.RETURN)

    time.sleep(25.0)

    try:
        close_banner=driver.find_element_by_id("_pendo-close-guide_")
        close_banner.click()
    except:
        None


    try:
        close_banner=driver.find_element_by_id("_pendo-close-guide_")
        close_banner.click()
    except:
        None

    numResults=driver.find_element_by_class_name("resultsCount").text
    sys.stdout.write("\nNumber of Publications found: "+str(numResults))

    if (int(numResults.replace(',', '')) > 2000):
        sys.stdout.write("\nNumber of Publications too big, > 2000! Change query")

    all_checkbox = driver.find_element_by_id("selectAllCheck")
    all_checkbox.click()

    export_results = driver.find_element_by_id('export_results')
    export_results.click()

    
    #csv_radio = driver.find_element_by_xpath('//ul[@id="exportList"]/li/input[@id="CSV"]')
    
    time.sleep(4.0)
    csv_radio = driver.find_element_by_xpath("//label[@for='CSV']")
    try:
        csv_radio.click()
    except: 
        print("csv_radio not clickable")
        csv_radio2 = driver.find_element_by_xpath("//input[@id='CSV']")
        try:
            csv_radio2.click()
        except: 
            print("csv_radio2 not clickable")        
    

    abstract_kw_checkbox = driver.find_element_by_id('abstractInformationCheckboxes')
    abstract_kw_checkbox.click()

    pathToSaveResult = pathToSaveResult

    export_trigger = driver.find_element_by_id('exportTrigger')
    export_trigger.click()

    time.sleep(20.0)
    
    fileNameDownloaded = 'scopus.csv'
    
    while not os.path.exists(pathToSaveResult + "/" + fileNameDownloaded):
        print("Waiting...")
        time.sleep(1)    
    
    os.rename(pathToSaveResult + "/" + fileNameDownloaded, pathToSaveResult + "/"+ perimeter + "_" + description + ".csv")
    
    sys.stdout.write("\nDownloaded")
    driver.close()